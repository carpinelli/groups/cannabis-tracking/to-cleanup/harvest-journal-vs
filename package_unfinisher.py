#! /usr/bin/env python3

"""Unfinish Harvest Journal packages back to a specified date."""

import logging
import traceback
import time
import datetime
from datetime import datetime
import random

import pandas as pd
import pyperclip
import pyautogui

from IPython.core.debugger import set_trace


# # # Globals / Logger # # #

# Logger Setup
try:
    LogFile = "package_unfinisher.log"
    LogFmt = "%(asctime)s - %(levelname)s - %(message)s"
    logger = logging.getLogger(__name__)  # Get logger with module name
    fileHndl = logging.FileHandler(LogFile, mode='a')  # Get file handler  # mode='a'
    streamHndl = logging.StreamHandler()  # Get stream handler
    formatter = logging.Formatter(LogFmt)  # Get formatter

    # Handle message levels
    logger.setLevel(logging.DEBUG)
    fileHndl.setLevel(logging.DEBUG)
    streamHndl.setLevel(logging.INFO)

    # Set formatter
    fileHndl.setFormatter(formatter)
    streamHndl.setFormatter(formatter)

    # Add file handlers
    logger.addHandler(fileHndl)
    logger.addHandler(streamHndl)

except RuntimeWarning:
    # Attempt to log setup failure
    print(traceback.format_exc())
    print("An exception occured... Logging disabled.")
    # Notify setup failure outside logging
    logging.disable()  # Disable logging after failed setup

Interval = 0.11
RelativeCoords = (50, 50)  # (x, y)
Unfinish_To = datetime(2019, 5, 15).date()

# Setup
random.seed(time.time())
# PyAutoGUI
# pyautogui.PAUSE = 0.4  # Give Windows/NAV time react
pyautogui.FAILSAFE = True  # Allow moving mouse to top-left corner to interupt program


# # # Functions # # #


def random_coordinates() -> tuple:
    """Gets screen size, then generates random coordinates on the screen."""
    random.seed(time.time())
    screensize = pyautogui.size()
    return (random.uniform(0, screensize[0]), random.uniform(0, screensize[1]))


def random_relative_coordinates(Range: tuple = (255, 255)) -> tuple:
    """Get random (x, y) coordinates in a given range."""
    random.seed(time.time())
    return (random.uniform(Range[0] * -1, Range[0]), random.uniform(Range[1] * -1, Range[1]))


def move_random(Range: tuple = (255, 255)) -> None:
    """Moves the mouse a random number of relative pixels based on a given range."""
    # Check if movement will trigger failsafe
    while True:
        relative_coordinates = random_relative_coordinates()  # Get new relative coordinates
        new_position = tuple(coord0 + coord1 for coord0, coord1 in zip(pyautogui.position(),
                                                                  relative_coordinates))
        if not new_position <= (1, 1):
            break
    pyautogui.moveRel(relative_coordinates, duration=0.24)
    return None


def wait_for(ImageFile: str, RelativeCoords: tuple = (50, 50), Wait: int = 0) -> tuple:
    """Waits for a PNG file to appear on screen, then returns the coordinates."""
    while True:
        coords = pyautogui.locateCenterOnScreen(ImageFile)
        if coords is not None:
            break
        move_random(RelativeCoords)
        time.sleep(Wait)
    return coords


def wait_click(ImageFile: str, RelativeCoords: tuple = (50, 50), Wait: int = 0) -> None:
    """Waits for PNG file to appear on screen, then clicks the coordinates."""
    pyautogui.click(wait_for(ImageFile, RelativeCoords, Wait))


def wait_click_move(ImageFile: str, RelativeCoords: tuple = (50, 50), Wait: int = 0) -> None:
    """Waits for PNG file to appear on screen, clicks the coordinates,
    then moves off the button."""
    wait_click(ImageFile, RelativeCoords, Wait)
    move_random(RelativeCoords)


def wait_rclick(ImageFile: str, RelativeCoords: tuple = (50, 50), Wait: int = 0) -> None:
    """Waits for PNG file to appear on screen, then right-clicks the coordinates."""
    pyautogui.click(wait_for(ImageFile, RelativeCoords, Wait), button="right")


# # # Main # # #


def main():
    # Open NAV
    time.sleep(1.0)  # Start Menu won't open if super is pressed immediately
    pyautogui.run("k'win' w'Dynamics NAV' s0.5 k'enter'")  # Wait 0.5 for Windows to catchup
    
    # Wait for NAV to load
    wait_click("PNG/harvestJournal.png")  # Open Harvest Journal
    
    # Wait for Harvest Journal to load then open finished packages
    wait_click("PNG/showFinishedPackages.png")
    
    # Wait for finished packages to load and fullscreen the window by dragging
    # it to the top of the screen
    coords = wait_for("PNG/finishedPackagesTitle.png")
    # pyautogui.run(f"g{coords[0]},{coords[1]} d{coords[0]},0")
    pyautogui.moveTo(coords)
    pyautogui.dragTo(coords[0], 0, duration=0.54)
    
    # Right-click to organize by date
    wait_rclick("PNG/creationDate.png")
    wait_click("PNG/sortDescending.png")
    wait_for("PNG/creationDateSorted.png")
    
    # Start Loop
    while True:
        pyautogui.press("uparrow", presses=500)  # 
        pyautogui.hotkey("ctrl", "shift", 'c')  # Get highlighted entry values
        # Load entry into pandas
        package_date = datetime.strptime((pd.read_clipboard(sep='\t')["Creation Date"][0]),
                                         "%m/%d/%Y").date()  # Get date entry

        if package_date < Unfinish_To:
            break

        wait_click_move("PNG/unfinishPackage.png")
        time.sleep(1)  # wait_for("PNG/confirmUnfinished.png")
        pyautogui.run("k'tab' s0.5 k'enter'")
        time.sleep(7.5)  # wait_for("PNG/packageUnfinished.png")
        pyautogui.press("enter")  # Confirm dialog prompt
        time.sleep(1.5)  # Wait for NAV to load
        logger.info(f"Unfinished... Package Date: {package_date}")

    print("Potentially finished, check me!")        
    set_trace()
    
    return


if __name__ == "__main__":
    try:
        main()
    except Exception:
        pyautogui.alert(traceback.format_exc())
        logger.critical(traceback.format_exc())
